# my-config-lxqt

- We are going to replace labwc with Wayfire. The last commit that will support labwc will be https://gitlab.com/manoel-linux1/my-config-lxqt/-/commit/2495ade2f44ddbefdcdd0a23dbede640660772c8. After this commit, we will transition to Wayfire.

- Only for wayland

<img src="https://gitlab.com/manoel-linux1/my-config-mate/-/raw/master/lxqt-wayland.png" align="center" width="720"/>