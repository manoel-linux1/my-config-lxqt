#!/bin/bash

sudo apk update && sudo apk upgrade

sudo apk add elogind polkit-elogind

sudo rc-update add elogind default

sudo rc-update add polkit default

sudo apk update && sudo apk upgrade

sudo apk add linux-firmware

sudo apk update && sudo apk upgrade

sudo apk add dbus mesa-vdpau-gallium mesa-va-gallium libva libva-vdpau-driver font-noto-cjk gvfs gvfs-mtp ntfs-3g exfatprogs e2fsprogs ffmpeg mesa-vulkan-ati vulkan-loader mesa-vulkan-layers wget unzip vim nano git intel-ucode amd-ucode qps networkmanager sudo wireless-tools os-prober mtools dosfstools pipewire pipewire-pulse wireplumber mesa mesa-dri-gallium f2fs-tools ca-certificates libdrm chrony networkmanager-cli networkmanager-wifi greetd-openrc greetd-agreety

sudo rc-update add chronyd default

sudo rc-update add greetd default

sudo apk update && sudo apk upgrade

sudo apk del dhcpcd