#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

sudo apk update && sudo apk upgrade

sudo apk del xf86-video-intel

sudo apk del xf86-video-nouveau

sudo apk del xf86-video-amdgpu

sudo apk del xf86-video-ati

sudo apk del xf86-video-vesa

sudo apk del xf86-video-fbdev

sudo apk update && sudo apk upgrade

sudo apk add wireplumber-logind foot xwayland wayfire wayfire-plugins-extra wf-shell kanshi grim slurp procps font-dejavu lxqt-config lxqt-panel lxqt-qtplugin lxqt-session pcmanfm-qt lxqt-policykit lximage-qt lxqt-themes lxqt-archiver qterminal featherpad featherpad-lang breeze-gtk breeze-icons vulkan-tools steam-devices 7zip xdg-user-dirs xdg-utils flatpak xdg-desktop-portal xdg-desktop-portal-gtk util-linux-misc

sudo apk update && sudo apk upgrade

sudo apk del xf86-video-intel

sudo apk del xf86-video-nouveau

sudo apk del xf86-video-amdgpu

sudo apk del xf86-video-ati

sudo apk del xf86-video-vesa

sudo apk del xf86-video-fbdev

sudo apk update && sudo apk upgrade

sudo apk add lang

sudo apk update && sudo apk upgrade

sudo apk fix

clear

flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

clear

echo "#################################################################"

rm -rf ~/.config/kanshi

rm -rf ~/.config/wayfire.ini

rm -rf ~/.config/wf-shell.ini

sudo rm -rf /usr/share/applications/restart.desktop

sudo rm -rf /etc/chrony/chrony.conf

sudo rm -rf /usr/bin/mini-opti

sudo rm -rf /usr/bin/opti-kernel

sudo rm -rf /usr/bin/opti-wayfire

sudo rm -rf /usr/bin/run-wayfire-panel

sudo rm -rf /usr/bin/scroll-lock-led

sudo rm -rf /usr/bin/startwayfire

sudo rm -rf /etc/sysctl.conf

sudo rm -rf /usr/bin/wayfire-screenshot

sudo rm -rf /usr/bin/wayfire-tweaks

echo "#################################################################"

clear

echo "#################################################################"

sudo cp desktops/* /usr/share/applications/

sudo cp chrony.conf /etc/chrony/

sudo cp mini-opti /usr/bin/

sudo cp opti-kernel /usr/bin/

sudo cp opti-wayfire /usr/bin/

sudo cp run-wayfire-panel /usr/bin/

sudo cp scroll-lock-led /usr/bin/

sudo cp startwayfire /usr/bin/

sudo cp sysctl.conf /etc/

sudo cp wayfire-screenshot /usr/bin/

sudo cp wayfire-tweaks /usr/bin/

echo "#################################################################"

clear

echo "#################################################################"

sudo chmod +x /usr/bin/mini-opti

sudo chmod +x /usr/bin/opti-kernel

sudo chmod +x /usr/bin/opti-wayfire

sudo chmod +x /usr/bin/run-wayfire-panel

sudo chmod +x /usr/bin/scroll-lock-led

sudo chmod +x /usr/bin/startwayfire

sudo chmod +x /usr/bin/wayfire-screenshot

sudo chmod +x /usr/bin/wayfire-tweaks

echo "#################################################################"

mkdir -p ~/.config/kanshi

cp kanshi/config ~/.config/kanshi/

cp wayfire.ini ~/.config/

cp wf-shell.ini ~/.config/

echo "#################################################################"

git clone https://gitlab.com/manoel-linux1/GlibMus-HQ.git

cd GlibMus-HQ

chmod a+x compile-x86_64-alpine-linux.sh

./compile-x86_64-alpine-linux.sh

cd ..

sudo rm -rf GlibMus-HQ

echo "#################################################################"

git clone https://gitlab.com/manoel-linux1/ydotool-for-musl.git

cd ydotool-for-musl

chmod a+x installupdate-alpine-linux.sh

./installupdate-alpine-linux.sh

cd ..

sudo rm -rf ydotool-for-musl

echo "#################################################################"

update-desktop-database

sudo update-desktop-database

echo "#################################################################"

clear

cd $HOME

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(SYSCTL)"
echo "#################################################################"
cat /etc/sysctl.conf